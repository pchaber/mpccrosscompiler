function code = PID(par)
    K  = par(1);
    Ti = par(2);
    Td = par(3);
    T  = par(4);
    
    code = [];
    code = [code sprintf('void %%s(ArchiveData * ad, CurrentControl * c){\n')];  	
    code = [code sprintf('\tstatic float e0;\n')];
	code = [code sprintf('\tstatic float e1;\n')];
	code = [code sprintf('\tstatic float e2;\n')];
	code = [code sprintf('\tif(ad == NULL) return;\n')];
	code = [code sprintf('\te0 = ad->yzad - ad->yk[M( 0)];\n')];
	code = [code sprintf('\te1 = ad->yzad - ad->yk[M(-1)];\n')];
	code = [code sprintf('\te2 = ad->yzad - ad->yk[M(-2)];\n')];
    code = [code sprintf('\tsetCurrentControlIncrement(c,%f*e0 + %f*e1 + %f*e2); // algorytm przyrostowy\n',K*(1 + T/(2*Ti) + Td/T),K*( T/(2*Ti) - 2*(Td/T) - 1),K*Td)];
    code = [code sprintf('}\n')];
end