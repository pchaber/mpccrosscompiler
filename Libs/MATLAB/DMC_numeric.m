function code = DMC_numeric(S,N,Nu,Lambda,dumin,dumax,umin,umax)
    if(nargin < 8) umax = []; end;
    if(nargin < 7) umin = []; end;
    if(nargin < 6) dumax = []; end;
    if(nargin < 5) dumin = []; end;
    str = @(x)(num2str(x,'%.15e'));
    D = length(S);    
    code = [];
    code = [code sprintf('#include "./Libs/pk.h"\n')];
    code = [code sprintf('void %%s(ArchiveData * ad, CurrentControl * c){\n')];
    
    code = [code sprintf('\tstatic long i,j,k;\n')];
    code = [code sprintf('\tstatic float du;\n')];
    code = [code sprintf('\tstatic float dUp[%d] = {0};\n',(D-1))];
    code = [code sprintf('\tstatic float Y[%d] = {0};\n',N)];
    code = [code sprintf('\tstatic float F[%d] = {0};\n',Nu)];
    code = [code sprintf('\tstatic float Y0[%d] = {0};\n',N)];
    code = [code sprintf('\tstatic float K1[%d] = {',N)];
    
    M = zeros(N,Nu);
    for i=1:N
        for j=1:Nu
            if(i>=j)
                M(i,j) = S(i-j+1);
            end
        end
    end

    K1 = (M'*M+Lambda)^(-1)*M';
    
    for i = 1:N
        code = [code sprintf('%s',str(K1(1,i)))];
        if(i < N)
            code = [code sprintf(',')];
        end
    end
    code = [code sprintf('};\n')];

    H = 2*(M'*M+Lambda);

    code = [code sprintf('\tstatic float Mp[%d][%d] = {',N,(D-1))];
    for i = 1:N
        code = [code sprintf('{')];
        for j = 1:(D-1)
            s_tmp1 = S(D);
            if((i+j)<D); s_tmp1 = S(i+j); end;
            s_tmp2 = S(j);
            code = [code sprintf('%s',str(s_tmp1-s_tmp2))];
            if(j<(D-1))
                code = [code sprintf(',')];
            end
        end
        code = [code sprintf('}')];
        if(i<N)
            code = [code sprintf(',')];
        end
    end
    code = [code sprintf('};\n')];
    code = [code sprintf('\tstatic float M[%d][%d] = {',N,Nu)];
    for i = 1:N
        code = [code sprintf('{')];
        for j = 1:Nu
            code = [code sprintf('%s',str(M(i,j)))];
            if(j<Nu)
                code = [code sprintf(',')];
            end
        end
        code = [code sprintf('}')];
        if(i<N)
            code = [code sprintf(',')];
        end
    end
    code = [code sprintf('};\n')];

    code = [code sprintf('\tstatic PkOptions opt;\n')];
    code = [code sprintf('\tif(ad == NULL){\n')];
    code = [code sprintf('\t\tinitPkOptions(&opt,%d,%d,1e-8,1e-10);\n',Nu,4*Nu)];
    % code = [code sprintf('\t\tinitPkOptions(&opt,%d,%d,1e-5,1e-6);\n',Nu,4*Nu)];
    
    for i = 1:Nu
        for j = 1:Nu;
            code = [code sprintf('\t\topt.G[%d][%d]=%s;\n',i,j,str(H(i,j)))];
        end
    end
    
    A = [];
    b = [];
    c = [];
    ltr = tril(ones(Nu));
    
    if(~isempty(dumin))
        A = [A; -eye(Nu)];
        b = [b; -1*dumin*ones(Nu,1)];
        c = [c; zeros(Nu,1)];
    end
    
    if(~isempty(dumax))
        A = [A;  eye(Nu)];
        b = [b;  1*dumax*ones(Nu,1)];
        c = [c; zeros(Nu,1)];
    end
    
    if(~isempty(umin))
        A = [A; -ltr];
        b = [b; -1*umin*ones(Nu,1)]; % + u(k-1)
        c = [c; 1*ones(Nu,1)];        
    end
    
    if(~isempty(umax))
        A = [A;  ltr];
        b = [b;  1*umax*ones(Nu,1)]; % - u(k-1)
        c = [c; -1*ones(Nu,1)];
    end
  
    A = -A;
    b = -b;
    c = -c;
    
    for i = 1:size(A,1)
        for j = 1:size(A,2)
            if(A(i,j) ~= 0)
                code = [code sprintf('\t\topt.A[%d][%d] = %f;\n',i,j,A(i,j))];
            end
        end
    end
    for i = 1:size(b,1)
        if(c(i) == 0)
            code = [code sprintf('\t\topt.b[%d] = %s;\n',i,str(b(i)))];
        else
            break
        end 
    end
    code = [code sprintf('\t\treturn;\n')];
    code = [code sprintf('\t}\n')];
    for i = i:size(b,1)
        if(c(i) == 1)
            code = [code sprintf('\topt.b[%d] = %s+ad->uk1[M(-1)];\n',i,str(b(i)))];
        else
            code = [code sprintf('\topt.b[%d] = %s-ad->uk1[M(-1)];\n',i,str(b(i)))];
        end
    end
    
%     code = [code sprintf('\t}\n')];
    
    code = [code sprintf('\tdu=0;\n')];
    
    code = [code sprintf('\tfor(i=1; i<=%d; ++i) dUp[M(i)] = ad->duk1[M(-i)]; \n',D-1)];
    code = [code sprintf('\tfor(i=1; i<=%d; ++i) Y[M(i)] = ad->yk[M(0)];\n',N)];
    code = [code sprintf('\tfor(i=1; i<=%d; ++i){\n',N)];
    code = [code sprintf('\t\tfloat tmp = 0;\n')];
    code = [code sprintf('\t\tfor(j=1; j<=%d; ++j){\n',(D-1))];
    code = [code sprintf('\t\t\ttmp += Mp[M(i)][M(j)]*dUp[M(j)];\n')];
    code = [code sprintf('\t\t}\n')];
    code = [code sprintf('\t\tY0[M(i)] = Y[M(i)]+tmp;\n')];
    code = [code sprintf('\t}\n')];

    code = [code sprintf('\tfor(i=1;i<=%d;++i){\n',Nu)];
    code = [code sprintf('\t\tF[M(i)] = 0;\n')];
    code = [code sprintf('\t\tfor(j=1;j<=%d;++j) F[M(i)] += -2*M[M(j)][M(i)]*(ad->yzad-Y0[M(j)]);\n',N)];
    code = [code sprintf('\t}\n')];
    
    
    code = [code sprintf('\tfor(i=1;i<=%d;++i){\n',Nu)];
    code = [code sprintf('\t\topt.t[i]=F[M(i)];\n')];
    code = [code sprintf('\t\topt.x[i]=0.0;\n')];
    code = [code sprintf('\t}\n')];
    code = [code sprintf('\tint ret = runPk(&opt);\n')];
    code = [code sprintf('\tif(ret == 1){ du = opt.x[1];\n')];
    code = [code sprintf('\t}else if(ret == 2){\n')];
    code = [code sprintf('\twrite_string_ext("Break!",0,1);\n')];
    if(~isempty(dumax) || ~isempty(dumin) || ~isempty(umax) || ~isempty(umin))
        code = [code sprintf('\t}else if(ret == -2){\n')];
        if(~isempty(dumax))	code = [code sprintf('\t\tif(du > %s) du=%s;\n',str(dumax),str(dumax))]; end;
        if(~isempty(dumin))	code = [code sprintf('\t\tif(du < %s) du=%s;\n',str(dumin),str(dumin))]; end;
        if(~isempty(umax))	code = [code sprintf('\t\tif(du > %s-ad->uk1[M(-1)])du=%s-ad->uk1[M(-1)];\n',str(umax),str(umax))]; end;
        if(~isempty(umin))	code = [code sprintf('\t\tif(du < %s-ad->uk1[M(-1)])du=%s-ad->uk1[M(-1)];\n',str(umin),str(umin))]; end;
    end
    code = [code sprintf('\t}else{\n')];
    code = [code sprintf('\t\tdu = 0.0;\n')];
    code = [code sprintf('\t\tsprintf(str,"Error: %%%%d",ret);\n')];
    code = [code sprintf('\t\twrite_string(str);\n')];
    code = [code sprintf('\t}\n')];
    
    code = [code sprintf('\tsetCurrentControlIncrement(c,du);\n')];
    code = [code sprintf('}\n')];
    
end