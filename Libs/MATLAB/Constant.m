function code = Constant(par)
    code = [];
    code = [code sprintf('void %%s(ArchiveData * ad, CurrentControl * c){\n')];  	
	code = [code sprintf('\tif(ad == NULL) return;\n')];
    code = [code sprintf('\tsetCurrentControl(c,%s); // algorytm przyrostowy\n',par)];
    code = [code sprintf('}\n')];
end