function code = GPC_analitic(A,B,N,Nu,Lambda,dumin,dumax,umin,umax)
    if(nargin < 8) umax = []; end;
    if(nargin < 7) umin = []; end;
    if(nargin < 6) dumax = []; end;
    if(nargin < 5) dumin = []; end;
    str = @(x)(num2str(x,'%.15e'));

    nb = length(B);
    na = length(A);
    
    code = [];
    code = [code sprintf('void %%s(ArchiveData * ad, CurrentControl * c){\n')];

    
    S = zeros(N,1);
    for j=1:N
        S(j) = 0;
        for i=1:min(j,nb)
            S(j) = S(j) + B(i);
        end
        for i=1:min(j-1,na)
            S(j) = S(j) - A(i)*S(j-i);
        end
    end

    
    code = [code sprintf('\tstatic long i,j,k;\n')];
    code = [code sprintf('\tstatic float du;\n')];
    code = [code sprintf('\tstatic float Y0[%d] = {0};\n',N)];
    code = [code sprintf('\tstatic float K1[%d] = {',N)];
    M = zeros(N,Nu);
    for i=1:N
        for j=1:Nu
            if(i>=j)
                M(i,j) = S(i-j+1);
            end
        end
    end

    K1 = (M'*M+Lambda)^(-1)*M';
    
    for i = 1:N
        code = [code sprintf('%s',str(K1(1,i)))];
        if(i < N)
            code = [code sprintf(',')];
        end
    end
    code = [code sprintf('};\n')];

	code = [code sprintf('\tif(ad == NULL) return;\n')];
	code = [code sprintf('\tfloat d = 0;\n')];
	code = [code sprintf('\tfloat ymod = 0;\n')];
    
    for i=1:na
        code = [code sprintf('\tymod -= %s*ad->yk[M(-%d)];\n',str(A(i)),i)];
    end
    for i=1:nb
        code = [code sprintf('\tymod += %s*ad->uk1[M(-%d)];\n',str(B(i)),i)];
    end
    code = [code sprintf('\td = ad->yk[M(0)] - ymod;\n')];
    
    
    for p=1:N
        code = [code sprintf('\tY0[M(%d)] = d;\n',p)];
        for i=1:na
            if(p-i > 0)
                code = [code sprintf('\tY0[M(%d)] -= %s*Y0[M(%d-%d)];\n',p,str(A(i)),p,i)];
            else 
                code = [code sprintf('\tY0[M(%d)] -= %s*ad->yk[M(%d-%d)];\n',p,str(A(i)),p,i)];
            end
        end
    
% 		for(p=1; p<=N; ++p){
% 			Y0[M(p)] = d;
% 			for(i=1; i<=na; ++i){
% 				if(p-i > 0){
% 					Y0[M(p)] -= A[M(i)]*Y0[M(p-i)];
% 				} else {
% 					Y0[M(p)] -= A[M(i)]*ad->yk[M(p-i)];
% 				}
% 			}    
        for i=1:nb
            if(p-i >= 0)
                code = [code sprintf('\tY0[M(%d)] += %s*ad->uk1[M(-1)];\n',p,str(B(i)))];
            else 
                code = [code sprintf('\tY0[M(%d)] += %s*ad->uk1[M(%d-%d)];\n',p,str(B(i)),p,i)];
            end
        end
    end
% 			for(i=1; i<=nb; ++i){
% 				if(p-i >= 0){
% 					Y0[M(p)] += B[M(i)]*ad->uk1[M(-1)];
% 				} else {
% 					Y0[M(p)] += B[M(i)]*ad->uk1[M(p-i)];
% 				}				
% 			}
% 		}
% 
% 		// dU = K1*[Yzad-Y0]
% 		du = 0.0;
% 		for(i=1;i<=N;++i){
% 			du += K1[M(i)]*(Yzad[M(i)]-Y0[M(i)]);
% 		}
% 		setCurrentControlIncrement(c,du);
% 		// printf("Nowe sterowanie: %30.20f\n", du);
% 	}
% }
    code = [code sprintf('\tdu=0;\n')];
    code = [code sprintf('\tfor(i=1; i<=%d; ++i) du += K1[M(i)]*(ad->yzad-Y0[M(i)]);\n',N)];
    if(~isempty(dumax))	code = [code sprintf('\tif(du > %s) du=%s;\n',str(dumax),str(dumax))]; end;
	if(~isempty(dumin))	code = [code sprintf('\tif(du < %s) du=%s;\n',str(dumin),str(dumin))]; end;
	if(~isempty(umax))	code = [code sprintf('\tif(du > %s-ad->uk1[M(-1)])du=%s-ad->uk1[M(-1)];\n',str(umax),str(umax))]; end;
	if(~isempty(umin))	code = [code sprintf('\tif(du < %s-ad->uk1[M(-1)])du=%s-ad->uk1[M(-1)];\n',str(umin),str(umin))]; end;
    
    code = [code sprintf('\tsetCurrentControlIncrement(c,du);\n')];
    code = [code sprintf('}\n')];
    
end