function code = DMC_analitic(S,N,Nu,Lambda,dumin,dumax,umin,umax)
    if(nargin < 8) umax = []; end;
    if(nargin < 7) umin = []; end;
    if(nargin < 6) dumax = []; end;
    if(nargin < 5) dumin = []; end;
    str = @(x)(num2str(x,'%.15e'));
    D = length(S);    
    code = [];
    code = [code sprintf('void %%s(ArchiveData * ad, CurrentControl * c){\n')];
    
    code = [code sprintf('\tstatic long i,j,k;\n')];
    code = [code sprintf('\tstatic float du;\n')];
    code = [code sprintf('\tstatic float dUp[%d] = {0};\n',(D-1))];
    code = [code sprintf('\tstatic float Y[%d] = {0};\n',N)];
    code = [code sprintf('\tstatic float Y0[%d] = {0};\n',N)];
    code = [code sprintf('\tstatic float K1[%d] = {',N)];
    
    M = zeros(N,Nu);
    for i=1:N
        for j=1:Nu
            if(i>=j)
                M(i,j) = S(i-j+1);
            end
        end
    end

    K1 = (M'*M+Lambda)^(-1)*M';
    
    for i = 1:N
        code = [code sprintf('%s',str(K1(1,i)))];
        if(i < N)
            code = [code sprintf(',')];
        end
    end
    code = [code sprintf('};\n')];

    code = [code sprintf('\tstatic float Mp[%d][%d] = {',N,(D-1))];
    for i = 1:N
        code = [code sprintf('{')];
        for j = 1:(D-1)
            s_tmp1 = S(D);
            if((i+j)<D); s_tmp1 = S(i+j); end;
            s_tmp2 = S(j);
            code = [code sprintf('%s',str(s_tmp1-s_tmp2))];
            if(j<(D-1))
                code = [code sprintf(',')];
            end
        end
        code = [code sprintf('}')];
        if(i<N)
            code = [code sprintf(',')];
        end
    end
    code = [code sprintf('};\n')];
	code = [code sprintf('\tif(ad == NULL) return;\n')];
    code = [code sprintf('\tdu=0;\n')];
    code = [code sprintf('\tfor(i=1; i<=%d; ++i) dUp[M(i)] = ad->duk1[M(-i)]; \n',D-1)];
    code = [code sprintf('\tfor(i=1; i<=%d; ++i) Y[M(i)] = ad->yk[M(0)];\n',N)];
    code = [code sprintf('\tfor(i=1; i<=%d; ++i){\n',N)];
    code = [code sprintf('\t\tfloat tmp = 0;\n')];
    code = [code sprintf('\t\tfor(j=1; j<=%d; ++j){\n',(D-1))];
    code = [code sprintf('\t\t\ttmp += Mp[M(i)][M(j)]*dUp[M(j)];\n')];
    code = [code sprintf('\t\t}\n')];
    code = [code sprintf('\t\tY0[M(i)] = Y[M(i)]+tmp;\n')];
    code = [code sprintf('\t}\n')];
    code = [code sprintf('\tfor(i=1; i<=%d; ++i) du += K1[M(i)]*(ad->yzad-Y0[M(i)]);\n',N)];
    if(~isempty(dumax))	code = [code sprintf('\tif(du > %s) du=%s;\n',str(dumax),str(dumax))]; end;
	if(~isempty(dumin))	code = [code sprintf('\tif(du < %s) du=%s;\n',str(dumin),str(dumin))]; end;
	if(~isempty(umax))	code = [code sprintf('\tif(du > %s-ad->uk1[M(-1)])du=%s-ad->uk1[M(-1)];\n',str(umax),str(umax))]; end;
	if(~isempty(umin))	code = [code sprintf('\tif(du < %s-ad->uk1[M(-1)])du=%s-ad->uk1[M(-1)];\n',str(umin),str(umin))]; end;
    
    code = [code sprintf('\tsetCurrentControlIncrement(c,du);\n')];
    code = [code sprintf('}\n')];
    
end