#include <stdlib.h>
#include <stdio.h>
#include "./defines.h"
#include "./mpctools.h"
#include "./simulated_signals.h"

#ifdef VERBOSE
	#define WRITE_FUN write_string(__func__)
#else
	#define WRITE_FUN 
#endif

ArchiveData ad;
CurrentControl cc;

__attribute__ ((weak)) void write_string(char * str) { printf("%s\n",str); }

__attribute__ ((weak)) long get_time()		         { WRITE_FUN; return 0; }
__attribute__ ((weak)) void hardware_setup()         { WRITE_FUN; }
__attribute__ ((weak)) void controller_setup()       { WRITE_FUN; init_archive_data(&ad,1,1,0,0,0);}
__attribute__ ((weak)) void sim_setup()       		 { WRITE_FUN; }
__attribute__ ((weak)) void measurements()           { WRITE_FUN; }
__attribute__ ((weak)) void loop()                   { WRITE_FUN; }
__attribute__ ((weak)) void controls()               { WRITE_FUN; }
__attribute__ ((weak)) void idle()                   { WRITE_FUN; }
__attribute__ ((weak)) void timeout()                { WRITE_FUN; exit(1); }

#ifdef SIMULATION
__attribute__ ((weak)) void sim_measurements()  { WRITE_FUN; SIMULATE_FUNCTION(&ad); }
__attribute__ ((weak)) void sim_controls()      { WRITE_FUN; pushCurrentControlsToArchiveData(&cc,&ad); }
#else
__attribute__ ((weak)) void sim_measurements()  { WRITE_FUN; }
__attribute__ ((weak)) void sim_control()       { WRITE_FUN; }
#endif

