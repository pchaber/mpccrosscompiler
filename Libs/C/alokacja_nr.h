#ifndef __ALOKACJA_NR_H__
#define __ALOKACJA_NR_H__

void piszkomunikat(char tekst[],int polecenie);
long *lwektor(long nl, long nh);
void lwektorkasuj(long *v, long nl, long nh);
float *dwektor(long nl, long nh);
void dwektorkasuj(float *v, long nl, long nh);
long **ltablica(long nrl, long nrh, long ncl, long nch);
void ltablicakasuj(long **m, long nrl, long nrh, long ncl, long nch);
float **dtablica(long nrl, long nrh, long ncl, long nch);
void dtablicakasuj(float **m, long nrl, long nrh, long ncl, long nch);
float ***dtablica3(long nrl, long nrh, long ncl, long nch, long ndl, long ndh);
void dtablica3kasuj(float ***t, long nrl, long nrh, long ncl, long nch,long ndl, long ndh);

#endif