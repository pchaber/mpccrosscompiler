#include <stdio.h>
#include <sys/time.h>
#include <signal.h>
#include "./defines.h"
#include "./profiler.h"
#include "./simulated_signals.h"

extern void timeout();
extern void sim_setup();
extern void controller_setup();
extern void sim_measurements();
extern void loop();
extern void sim_controls();
extern void idle();

#ifndef ITERATION_TIME
	#define ITERATION_TIME 1000
#endif

#ifndef MAX_TIME
	#define MAX_TIME -1
#endif

#ifdef SIMULATION 
	#ifdef HARDWARE_SIMULATION
		#define HSIM // hardware simulation
	#else
		#define SSIM // software simulation
	#endif
#endif

volatile int ready = -1;

void timer_loop(){
#ifdef MPC_INTERRUPT
#ifndef TAKE_YOUR_TIME
	if(ready == -1) return;
	if(ready == 0)	ready = 1;
	else timeout();
#endif
#endif
}

int main_interrupt(void){	
	long t = 0;
	#ifndef SSIM
	hardware_setup();
	#else
	sim_setup();
	#endif
	controller_setup();
	ready = 1;
	while(1){
		#ifndef TAKE_YOUR_TIME
		while(ready == 0);
		#endif
		#ifndef SSIM
		measurements();
		#endif
		#ifdef SIMULATION
		sim_measurements();
		#endif
		loop();
		#ifndef SSIM
		controls();
		#else
		sim_controls();
		#endif
		idle();
		ready = 0;
		if((MAX_TIME >= 0) && get_time() > MAX_TIME) break;
	}
	timeout();
	return 0;
}

int main_counter(void){
	long t = 0;

	#ifndef SSIM
	hardware_setup();
	#else
	sim_setup();
	#endif
	controller_setup();

	while (1){
		t = get_time();
		#ifndef SSIM
		measurements();
		#endif
		#ifdef SIMULATION
		sim_measurements();
		#endif
		loop();
		#ifndef SSIM
		controls();
		#else
		sim_controls();
		#endif

		idle();
		if((MAX_TIME >= 0) && get_time() > MAX_TIME) break;
		#ifndef TAKE_YOUR_TIME
		if((get_time()-t) > ITERATION_TIME) timeout();
		#endif
		while((get_time()-t)<=ITERATION_TIME);
	}
	timeout();
	return 0;
}

int main(){
#ifdef MPC_INTERRUPT
	return main_interrupt();
#else
	return main_counter();
#endif
}
