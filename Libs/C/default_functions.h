#ifndef __DEFAULT_FUNCTIONS_H__
#define __DEFAULT_FUNCTIONS_H__

#include "./mpctools.h"

ArchiveData ad;
CurrentControl cc;

void write_string(char * str);
long get_time();
void hardware_setup();
void controller_setup();
void sim_setup();
void measurements();
void loop();
void controls();
void idle();
void timeout();

void sim_measurements();
void sim_controls();

#endif