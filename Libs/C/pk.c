#include <stdio.h>
#include <math.h>
#include "./pk.h"
#include "./alokacja_nr.h"
#include "./obl_macierzowe.h"

#ifndef MAXPKITER 
	#define MAXPKITER 100
#endif

static long ilzm,ilogr,ilor,*nrnzal;
static float *xr,*lambdar,**Gr,*tr,**S,**Z,**Ar,*br;						
static float **qr_a,**qr_q,**qr_r;											
static long ilograkt,*akt,*nrakt;											
static float **A,*b,*x,*lambda,**G,*t,epsogr,zero;							
static float *wp1,*wp2,*wp3;												
static float **mp1,**mp2,**mp3;											
static float **kopiama_lu,**ml_lu,**iloczyn_lu,*wy_lu,*wx_lu,**pusta_lu;	
static long *ind_lu;														
float rozklu(float **mac,long wym,float **macl,long *ind,float *znak)
{
	float wartnajw,liczba,wynik;
	long i,j,w,nrnajw;
	profiler_start(0x81);

	*znak=1.0;
	for(i=1;i<=wym;i++){
		for(j=1;j<=wym;j++){
			macl[i][j]=0.0;
			if(i==j){
				macl[i][j]=1.0;
			}
		}
		ind[i]=i;
	}
	for(w=1;w<=wym-1;w++){
		wartnajw=0.0; nrnajw=w;
		for(i=w;i<=wym;i++){
			if( fabs(mac[i][w])>wartnajw ){
				wartnajw=fabs(mac[i][w]);
				nrnajw=i;
			}
		}
		if(nrnajw==w){
		}else{
			*znak=*znak*(-1.0);
			for(i=1;i<=wym;i++){
				liczba=mac[w][i];
				mac[w][i]=mac[nrnajw][i];
				mac[nrnajw][i]=liczba;
			}
			j=ind[w]; ind[w]=ind[nrnajw]; ind[nrnajw]=j;
			for(j=1;j<=w-1;j++){
				liczba=macl[w][j];
				macl[w][j]=macl[nrnajw][j];
				macl[nrnajw][j]=liczba;
			}
		}
		for(i=w+1;i<=wym;i++){
			if(fabs(mac[w][w])>zero){
				macl[i][w]=mac[i][w]/mac[w][w];
			}else{
				macl[i][w]=0.0;
			}
		}
		for(i=w+1;i<=wym;i++){
			for(j=1;j<=wym;j++){
				mac[i][j]=mac[i][j]-mac[w][j]*macl[i][w];
			}
		}
	}
	wynik=*znak;
	for(i=1;i<=wym;i++){
		wynik=wynik*mac[i][i];
	}
	
	profiler_end(0x81);
	return(wynik);
}
long liczwyznmacodwr(float **ma,long wym,float **ma1,float *wyzn,int tryb){
	float suma,znak,wyznacznik;
	float b;
	long i,j,k,l;		
	for(i=1;i<=wym;i++){
		for(j=1;j<=wym;j++){
			kopiama_lu[i][j]=ma[i][j];
		}
	}
	wyznacznik=rozklu(ma,wym,ml_lu,ind_lu,&znak);
	*wyzn=wyznacznik;
	if(fabs(wyznacznik)>zero && tryb==1){
		for(k=1;k<=wym;k++){
			if(k==1){
				wy_lu[1]=1.0/ml_lu[1][1];
			}else{
				wy_lu[1]=0.0;
			}
			for(i=2;i<=wym;i++){
				if(i==k){
					b=1.0;
				}else{
					b=0.0;
				}
				wy_lu[i]=0.0;
				for(j=1;j<=i-1;j++){
					wy_lu[i]=wy_lu[i]+ml_lu[i][j]*wy_lu[j];
				}
				wy_lu[i]=(b-wy_lu[i])/ml_lu[i][i];
			}
			wx_lu[wym]=wy_lu[wym]/ma[wym][wym];
			for(i=wym-1;i>=1;i--){
				wx_lu[i]=0.0;
				for(j=i+1;j<=wym;j++){
					wx_lu[i]=wx_lu[i]+ma[i][j]*wx_lu[j];
				}
				wx_lu[i]=(wy_lu[i]-wx_lu[i])/ma[i][i];
			}
			for(i=1;i<=wym;i++){
				ma1[i][ind_lu[k]]=wx_lu[i];
			}
		}
		for(i=1;i<=wym;i++){
			for(j=1;j<=wym;j++){
				ma[i][j]=kopiama_lu[i][j];
			}
		}
		for(i=1;i<=wym;i++){
			for(j=1;j<=wym;j++){
				iloczyn_lu[i][j]=0.0;
				for(k=1;k<=wym;k++){
					iloczyn_lu[i][j]=iloczyn_lu[i][j]+ma[i][k]*ma1[k][j];
				}
			}
		}
	}
	for(i=1;i<=wym;i++){
		for(j=1;j<=wym;j++){
			ma[i][j]=kopiama_lu[i][j];
		}
	}
	if(fabs(wyznacznik)<zero && tryb==1){
		return(0);
	}else{
		return(1);
	}
	if(tryb==0){
		return(1);
	}
}
float liczwyzn(float **ma,long wym)
{
	float wyzn;
	liczwyznmacodwr(ma,wym,pusta_lu,&wyzn,0);
	return(wyzn);
}
float znak(float x)
{
	if(x>=0.0){
		return(1.0);
	}else{
		return(-1.0);
	}
}
void rozkqr(long ilw,long ilk)
{
	long i,j,k,l;
	float na,nu,K;
	profiler_start(0x83);

	if(ilw>ilk){
		for(i=1;i<=ilw;i++){
			for(j=ilk+1;j<=ilw;j++){
				qr_a[i][j]=0.0;
			}
		}
	}
	for(k=1;k<=ilw-1;k++){
		na=0.0;
		for(i=k;i<=ilzm;i++){
			if(k==1){
				wp1[i-k+1]=qr_a[i][k];
				na=na+wp1[i-k+1]*wp1[i-k+1];
			}else{
				wp1[i-k+1]=qr_r[i][k];
				na=na+wp1[i-k+1]*wp1[i-k+1];
			}
		}
		na=sqrt(na);
		nu=0.0;
		wp1[1]=wp1[1]+znak(wp1[1])*na;
		for(i=k;i<=ilzm;i++){
			nu=nu+wp1[i-k+1]*wp1[i-k+1];
		}
		if(nu>1.0e-10){
			K=0.5*nu;
		}else{
			nu=0.0;
		}
		nu=sqrt(nu);
		iloczynwwt(wp1,wp1,mp1,ilzm-k+1,ilzm-k+1);
		for(i=1;i<=ilzm-k+1;i++){
			for(j=1;j<=ilzm-k+1;j++){
				if(nu!=0.0){
					mp1[i][j]=-mp1[i][j]/K;
				}else{
					mp1[i][j]=0.0;
				}
				if(i==j){
					mp1[i][j]=mp1[i][j]+1.0;
				}
			}
		}
		if(k==1){
			iloczynab(mp1,qr_a,qr_r,ilzm,ilzm,ilzm,ilzm);
		}else{
		
			for(i=1;i<=ilzm;i++){
				for(j=1;j<=ilzm;j++){
					if(i>=k && j>=k){
						mp3[i][j]=0.0;
						for(l=1;l<=ilzm-k+1;l++){
							mp3[i][j]=mp3[i][j]+mp1[i-k+1][l]*qr_r[l+k-1][j];
						}
					}else{
						mp3[i][j]=qr_r[i][j];
					}
				}
			}
			for(i=1;i<=ilzm;i++){
				for(j=1;j<=ilzm;j++){
					qr_r[i][j]=mp3[i][j];
				}
			}
		}
		if(k==1){
			for(i=1;i<=ilzm;i++){
				for(j=1;j<=ilzm;j++){
					qr_q[i][j]=mp1[i][j];
				}
			}
		}else{
		
			for(i=1;i<=ilzm;i++){
				for(j=1;j<=ilzm;j++){
					if(i<=k-1 && j<=k-1){
						mp3[i][j]=qr_q[i][j];
					}else{
						mp3[i][j]=0.0;
						if(j<k){
							mp3[i][j]=qr_q[i][j];
						}else{
							for(l=1;l<=ilzm-k+1;l++){
								mp3[i][j]=mp3[i][j]+qr_q[i][l+k-1]*mp1[l][j-k+1];
							}
						}
					}
				}
			}
			for(i=1;i<=ilzm;i++){
				for(j=1;j<=ilzm;j++){
					qr_q[i][j]=mp3[i][j];
				}
			}
		}
	}
	if(ilw==1){
		for(i=1;i<=ilk;i++){
			qr_q[1][i]=1.0; qr_r[1][i]=qr_a[1][i];
		}
	}
	iloczynab(qr_q,qr_r,mp1,ilzm,ilzm,ilzm,ilzm);
	iloczynab(qr_q,qr_r,mp1,ilzm,ilzm,ilzm,ilzm);
	iloczynabt(qr_q,qr_q,mp1,ilzm,ilzm,ilzm,ilzm);
	profiler_end(0x83);
}
int pkr(void){
	long i,j,k,l,kasowanie,ilogrskas;
	float wyzn,licz,mian;
	kasowanie=0;
	if(ilor!=0){
		iloczynaat(Ar,mp1,ilor,ilzm);
		ilogrskas=0;
		wyzn=liczwyzn(mp1,ilor);
		if(fabs(wyzn)<zero){
			kasowanie=1;
			for(i=1;i<=ilzm;i++){
				mp1[1][i]=Ar[1][i];
				wp1[1]=br[1];
				nrnzal[1]=1;
			}
			k=1;
			for(l=2;l<=ilor;l++){
				for(i=1;i<=ilzm;i++){
					mp1[k+1][i]=Ar[l][i];
				}
				iloczynaat(mp1,mp2,k+1,ilzm);
				wyzn=liczwyzn(mp2,k+1);
				if(fabs(wyzn)<zero){
					nrnzal[l]=-l;
					ilogrskas++;
				}else{
					wp1[k+1]=br[l];
					nrnzal[l]=l;
					k++;
				}
			}
			ilor=k;
			for(i=1;i<=ilor;i++){
				br[i]=wp1[i];
				for(j=1;j<=ilzm;j++){
					Ar[i][j]=mp1[i][j];
				}
			}
		}
	}
	if(ilor==0){
		wyzn=liczwyzn(Gr,ilzm);
		if(fabs(wyzn)<zero){
			for(i=1;i<=ilzm;i++){
				for(j=1;j<=ilzm;j++){
					if(i!=j){
						if(fabs(Gr[i][j])>zero){
							return(-1);
						}
					}
				}
			}
			for(i=1;i<=ilzm;i++){
				if(fabs(Gr[i][i])>zero){
					xr[i]=-tr[i]/Gr[i][i];
				}else{
					xr[i]=0.0;
				}
			}
		}else{
			liczwyznmacodwr(Gr,ilzm,mp1,&wyzn,1);
			iloczynaw(mp1,tr,xr,ilzm,ilzm,ilzm);
			for(i=1;i<=ilzm;i++){
				xr[i]=-xr[i];
			}
		}
		return(1);
	}
	if(ilor!=0){
		if(ilor==ilzm){
			if(fabs(liczwyzn(Ar,ilzm))<zero){
				return(-1);
			}
			liczwyznmacodwr(Ar,ilzm,mp1,&wyzn,1);
			iloczynaw(mp1,br,xr,ilzm,ilzm,ilzm);
		}
		for(i=1;i<=ilor;i++){
			for(j=1;j<=ilzm;j++){
				qr_a[j][i]=Ar[i][j];
			}
		}
		rozkqr(ilzm,ilor);
		if(fabs(liczwyzn(qr_r,ilor))<zero){
			return(-1);
		}
		liczwyznmacodwr(qr_r,ilor,mp1,&wyzn,1);
		iloczynabt(qr_q,mp1,S,ilzm,ilor,ilor,ilor);
		if(ilor!=ilzm){
			for(i=1;i<=ilzm;i++){
				for(j=1;j<=ilzm-ilor;j++){
					Z[i][j]=qr_q[i][ilor+j];
				}
			}
			iloczynaw(S,br,wp1,ilzm,ilor,ilor);
			iloczynatb(Z,Gr,mp1,ilzm,ilzm-ilor,ilzm,ilzm);
			iloczynab(mp1,Z,mp2,ilzm-ilor,ilzm,ilzm,ilzm-ilor);
			if(fabs(liczwyzn(mp2,ilzm-ilor))<zero){
				return(-1);
			}
			liczwyznmacodwr(mp2,ilzm-ilor,mp1,&wyzn,1);
			iloczynab(Z,mp1,mp2,ilzm,ilzm-ilor,ilzm-ilor,ilzm-ilor);
			iloczynabt(mp2,Z,mp1,ilzm,ilzm-ilor,ilzm,ilzm-ilor);
			iloczynaw(Gr,wp1,wp2,ilzm,ilzm,ilzm);
			sumaww(tr,wp2,wp2,ilzm,1);
			iloczynaw(mp1,wp2,wp3,ilzm,ilzm,ilzm);
			sumaww(wp1,wp3,xr,ilzm,-1);
		}
		iloczynaw(Gr,xr,wp1,ilzm,ilzm,ilzm);
		sumaww(tr,wp1,wp2,ilzm,1);
		iloczynatw(S,wp2,lambdar,ilzm,ilor,ilzm);
		if(kasowanie==1){
			for(i=1;i<=ilor;i++){
				wp1[i]=lambdar[i];
			}
			j=1;
			for(i=1;i<=ilor+ilogrskas;i++){
				if(nrnzal[i]>0){
					lambdar[i]=wp1[j];
					j++;
				}else{
					lambdar[i]=0.0;
				}
			}
			ilor=ilor+ilogrskas;
		}
		return(1);
	}
}
void destroyPkOptions(PkOptions * opt){
	if(opt->A	 	!= NULL)	dtablicakasuj(opt->A,1,opt->ilogr,1,opt->ilzm);
	if(opt->b	 	!= NULL)	dwektorkasuj(opt->b,1,opt->ilogr);
	if(opt->x	 	!= NULL)	dwektorkasuj(opt->x,1,opt->ilzm);
	if(opt->lambda 	!= NULL)	dwektorkasuj(opt->lambda,1,opt->ilogr);
	if(opt->G 		!= NULL)	dtablicakasuj(opt->G,1,opt->ilzm,1,opt->ilzm);
	if(opt->t 		!= NULL)	dwektorkasuj(opt->t,1,opt->ilzm);
	opt->A = NULL;
	opt->b = NULL;
	opt->x = NULL;
	opt->lambda = NULL;
	opt->G = NULL;
	opt->t = NULL;
}
void initPkOptions(PkOptions * opt, long ilzm, long ilogr, float epsogr, float zero){
	destroyPkOptions(opt);
	long i = 0;
	long j = 0;
	opt->ilzm = ilzm;
	opt->ilogr = ilogr;
	opt->epsogr = epsogr;
	opt->zero = zero;
	opt->A=dtablica(1,ilogr,1,ilzm);
	opt->b=dwektor(1,ilogr);
	for(i = 1; i <= ilogr; ++i){
		for(j = 1; j <= ilzm; ++j){
			opt->A[i][j] = 0;
		}
		opt->b[i] = -1;
	}
	opt->x=dwektor(1,ilzm);
	opt->G=dtablica(1,ilzm,1,ilzm);
	opt->t=dwektor(1,ilzm);
	opt->lambda=dwektor(1,ilogr);
}
void inicjalizacjapk(PkOptions * opt)
{
	koniecpk(opt);
	zero = opt->zero;
	epsogr = opt->epsogr;
	ilzm = opt->ilzm;
	ilogr = opt->ilogr;
	long il;
	if(ilzm>ilogr){
		il=ilzm;
	}else{
		il=ilogr;
	}
	
	qr_a=dtablica(1,ilzm,1,ilzm);
	qr_q=dtablica(1,ilzm,1,ilzm);
	qr_r=dtablica(1,ilzm,1,ilzm);
	xr=dwektor(1,ilzm);
	lambdar=dwektor(1,ilogr);
	Gr=dtablica(1,ilzm,1,ilzm);
	tr=dwektor(1,ilzm);
	Ar=dtablica(1,ilogr,1,ilzm);
	br=dwektor(1,ilogr);
	S=dtablica(1,ilzm,1,ilzm);
	Z=dtablica(1,ilzm,1,ilzm);
	nrnzal=lwektor(1,ilogr);
	
	A=opt->A; 
	b=opt->b; 
	x=opt->x; 
	G=opt->G; 
	t=opt->t; 
	lambda=opt->lambda; 
	akt=lwektor(1,ilogr);
	nrakt=lwektor(1,ilogr);
	wp1=dwektor(1,il);
	wp2=dwektor(1,il);
	wp3=dwektor(1,il);
	mp1=dtablica(1,il,1,il);
	mp2=dtablica(1,il,1,il);
	mp3=dtablica(1,il,1,il);
	
	
	kopiama_lu=dtablica(1,il,1,il);
	ml_lu=dtablica(1,il,1,il);
	iloczyn_lu=dtablica(1,il,1,il);
	ind_lu=lwektor(1,il);
	wx_lu=dwektor(1,il);
	wy_lu=dwektor(1,il);
	pusta_lu=dtablica(1,il,1,il);
}
void koniecpk(PkOptions * opt)
{
	long il;
	if(ilzm>ilogr){
		il=ilzm;
	}else{
		il=ilogr;
	}
	if(qr_a		!= NULL)	dtablicakasuj(qr_a,1,ilzm,1,ilzm);
	if(qr_q		!= NULL)	dtablicakasuj(qr_q,1,ilzm,1,ilzm);
	if(qr_r		!= NULL)	dtablicakasuj(qr_r,1,ilzm,1,ilzm);
	if(xr	 	!= NULL)	dwektorkasuj(xr,1,ilzm);
	if(lambdar	!= NULL)	dwektorkasuj(lambdar,1,ilogr);
	if(Gr	 	!= NULL)	dtablicakasuj(Gr,1,ilzm,1,ilzm);
	if(tr	 	!= NULL)	dwektorkasuj(tr,1,ilzm);
	if(Ar	 	!= NULL)	dtablicakasuj(Ar,1,ilogr,1,ilzm);
	if(br	 	!= NULL)	dwektorkasuj(br,1,ilogr);
	if(S	 	!= NULL)	dtablicakasuj(S,1,ilzm,1,ilzm);
	if(Z	 	!= NULL)	dtablicakasuj(Z,1,ilzm,1,ilzm);
	if(nrnzal	!= NULL)	lwektorkasuj(nrnzal,1,ilogr);
	
	
	
	
	
	
	if(akt			!= NULL)	lwektorkasuj(akt,1,ilogr);
	if(nrakt		!= NULL)	lwektorkasuj(nrakt,1,ilogr);
	if(wp1		 	!= NULL)	dwektorkasuj(wp1,1,il);
	if(wp2		 	!= NULL)	dwektorkasuj(wp2,1,il);
	if(wp3		 	!= NULL)	dwektorkasuj(wp3,1,il);
	if(mp1		 	!= NULL)	dtablicakasuj(mp1,1,il,1,il);
	if(mp2		 	!= NULL)	dtablicakasuj(mp2,1,il,1,il);
	if(mp3		 	!= NULL)	dtablicakasuj(mp3,1,il,1,il);
	if(kopiama_lu	!= NULL)	dtablicakasuj(kopiama_lu,1,il,1,il);
	if(ml_lu		!= NULL)	dtablicakasuj(ml_lu,1,il,1,il);
	if(iloczyn_lu	!= NULL)	dtablicakasuj(iloczyn_lu,1,il,1,il);
	if(ind_lu		!= NULL)	lwektorkasuj(ind_lu,1,il);
	if(wx_lu		!= NULL)	dwektorkasuj(wx_lu,1,il);
	if(wy_lu		!= NULL)	dwektorkasuj(wy_lu,1,il);	
	if(pusta_lu		!= NULL)	dtablicakasuj(pusta_lu,1,il,1,il);
	qr_a = NULL;
	qr_q = NULL;
	qr_r = NULL;
	xr = NULL;
	lambdar = NULL;
	Gr = NULL;
	tr = NULL;
	Ar = NULL;
	br = NULL;
	S = NULL;
	Z = NULL;
	nrnzal = NULL;
	akt = NULL;
	nrakt = NULL;
	wp1 = NULL;
	wp2 = NULL;
	wp3 = NULL;
	mp1 = NULL;
	mp2 = NULL;
	mp3 = NULL;
	kopiama_lu = NULL;
	ml_lu = NULL;
	iloczyn_lu = NULL;
	ind_lu = NULL;
	wx_lu = NULL;
	wy_lu = NULL;
	pusta_lu = NULL;
}
int runPk(PkOptions * opt){
	long i,it;
	inicjalizacjapk(opt);
	i = pk();
	koniecpk(opt);
	return i;
}
int testogr(void)
{
	float suma;
	long i,j,k;
	ilograkt=0;
	for(i=1;i<=ilogr;i++){
		suma=0.0;
		for(j=1;j<=ilzm;j++){
			suma=suma+A[i][j]*x[j];
		}
		suma=suma-b[i];
		if(suma<-epsogr){ 
			return(0);
		}else{
			if(fabs(suma)<=epsogr){
				ilograkt++;
				akt[i]=1;
			}else{
				akt[i]=0;
			}
		}
	}
	k=1; 
	for(i=1;i<=ilogr;i++){
		if(akt[i]==1){
			nrakt[k]=i;
			k++;
		}
	}
	return(1);
}
int pomocniczezadpkr()
{
	long i,j;
	for(i=1;i<=ilzm;i++){
		for(j=1;j<=ilzm;j++){
			Gr[i][j]=G[i][j];
		}
	}
	for(i=1;i<=ilzm;i++){
		tr[i]=0.0;
		for(j=1;j<=ilzm;j++){
			tr[i]=tr[i]+Gr[i][j]*x[j];
		}
		tr[i]=tr[i]+t[i];
	}
	ilor=ilograkt;
	for(i=1;i<=ilor;i++){
		br[i]=0.0;
		for(j=1;j<=ilzm;j++){
			Ar[i][j]=A[nrakt[i]][j];
		}
	}
	return(pkr());
}
int pk()
{
	long i,j,nrlmin,jestalfa,nralfamin;
	float norma,lmin,alfa,mian,licznik;
	long iteracja = MAXPKITER;
	if(testogr()==0){
		return(-2);
	}
	for(;iteracja > 0; --iteracja){
	// while(1){
		profiler_start(0x80);

		if(pomocniczezadpkr()==-1){
			profiler_end(0x80);
			return(-1);
		}
		norma=0.0;
		for(i=1;i<=ilzm;i++){
			norma=norma+xr[i]*xr[i];
		}
		if(norma<=zero){
			lmin=0.0;
			for(i=1;i<=ilor;i++){
				if(lambdar[i]<lmin){
					lmin=lambdar[i];
					nrlmin=nrakt[i];
				}
			}
			if(lmin>=0.0){
				for(i=1;i<=ilogr;i++){
					lambda[i]=0.0;
				}
				for(i=1;i<=ilor;i++){
					lambda[nrakt[i]]=lambdar[i];
				}
				profiler_end(0x80);
				return(1);
			}
			akt[nrlmin]=0;
			ilograkt=ilograkt-1;
			j=1; 
			for(i=1;i<=ilogr;i++){
				if(akt[i]==1){
					nrakt[j]=i;
					j++;
				}
			}
			if(pomocniczezadpkr()==-1){
				profiler_end(0x80);
				return(-1);
			}
		}
		jestalfa=0;
		for(i=1;i<=ilogr;i++){
			if(akt[i]==0){
				mian=0.0;
				for(j=1;j<=ilzm;j++){
					mian=mian+A[i][j]*xr[j];
				}
				if(mian<0){
					licznik=b[i];
					for(j=1;j<=ilzm;j++){
						licznik=licznik-A[i][j]*x[j];;
					}
					if(jestalfa==0){
						alfa=licznik/mian;
						jestalfa=1;
						nralfamin=i;
					}else{
						if((licznik/mian)<alfa){
							alfa=licznik/mian;
							nralfamin=i;
						}
					}
				}else{
				}
			}else{
			}
		}
		if(jestalfa==1){
			alfa=fmin(1.0,alfa);
		}else{
			alfa=1.0;
		}
		for(i=1;i<=ilzm;i++){
			x[i]=x[i]+alfa*xr[i];
		}
		if(alfa<1.0){
			akt[nralfamin]=1;
			ilograkt++;
			j=1; 
			for(i=1;i<=ilogr;i++){
				if(akt[i]==1){
					nrakt[j]=i;
					j++;
				}
			}
		}else{
		}
		profiler_end(0x80);
	}
	return(2);
}
