#ifndef __PK_H__
#define __PK_H__

#include <math.h>
#include <stdio.h>
#include <stdlib.h>
#include "./alokacja_nr.h"
#include "./obl_macierzowe.h"


typedef struct {
	long ilzm;		// liczba zmiennych
	long ilogr;		// liczba ograniczen

	float **G;		// wspolczynniki funkcji celu stojace przy czynnikach kwadratowych
	float *t;		// wspolczynniki funkcji celu stojace przy czynnikach liniowych
	float **A;		// lewa strona ograniczen nierownosciowych
	float *b;		// prawa strona ograniczen nierownosciowych
	float *x;		// punkt startowy / wspolrzedne minimum
	float *lambda;	// wspolczynniki lambda odpowiadajace wspolrzednym minimum
	float epsogr;	// cos do ograniczen - moze sie przyda, moze nie...
	float zero;
} PkOptions;

float rozklu(float **mac,long wym,float **macl,long *ind,float *znak);
long liczwyznmacodwr(float **ma,long wym,float **ma1,float *wyzn,int tryb);
float liczwyzn(float **ma,long wym);
float znak(float x);
void rozkqr(long ilw,long ilk);
int pkr(void);

void initPkOptions(PkOptions * opt, long ilzm, long ilogr, float epsogr, float zero);
void inicjalizacjapk(PkOptions * opt);
void koniecpk(PkOptions * opt);
int runPk(PkOptions * opt);
int testogr(void);
int pomocniczezadpkr();
int pk();

#endif