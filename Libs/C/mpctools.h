#ifndef __MPCTOOLS_H__
#define __MPCTOOLS_H__

#ifndef MIN
	#define MIN(x,y) ((x)<(y)?(x):(y))
#endif

#ifndef ABS
	#define ABS(x) ((x)<0?(-(x)):(x))
#endif

#ifndef M
	#define M(x) ((x)-1)
#endif

typedef struct {
	float *    u; // będę tutaj trzymał wartości: ...,  u[k-2],  u[k-1]
	float *   du; // będę tutaj trzymał wartości: ..., du[k-2], du[k-1]
	float *    y; // będę tutaj trzymał wartości: ...,  y[k-2],  y[k-1],  y[k]
	float *  uk1; // uk[M(-1)] - wskaźnik na ostatni element
	float * duk1; // uk[M(-1)] - wskaźnik na ostatni element
	float *  yk ; // yk[M( 0)] - wskaźnik na ostatni element
	float yzad;
	long number_of_u;
	long number_of_y;
} ArchiveData, *ArchiveDataPtr;

typedef enum {ABS, INC} CCType;

typedef struct {
	CCType type;
	float u;
	float du;
} CurrentControl, *CurrentControlPtr;

void init_archive_data(ArchiveDataPtr ad, long number_of_u, long number_of_y, float default_u, float default_y, float current_yzad);
void new_control(ArchiveDataPtr ad, float u);
void new_control_increment(ArchiveDataPtr ad, float du);
void new_output(ArchiveDataPtr ad, float y);
void new_simulated_signal(ArchiveDataPtr ad, float y);
void setCurrentControlIncrement(CurrentControlPtr c,float du);
void setCurrentControl(CurrentControlPtr c,float u);
void pushCurrentControlsToArchiveData(CurrentControlPtr c,ArchiveDataPtr ad);
void CST_control(ArchiveDataPtr ad, CurrentControlPtr c);
void projectOnFeasibleSet(ArchiveDataPtr ad, float dumin, float dumax, float umin, float umax);
void projectControlsOnFeasibleSet(ArchiveDataPtr ad, float umin, float umax);
void projectControlsIncrementOnFeasibleSet(ArchiveDataPtr ad, float dumin, float dumax);
float last_control(ArchiveDataPtr ad);
float last_control_increment(ArchiveDataPtr ad);

#endif