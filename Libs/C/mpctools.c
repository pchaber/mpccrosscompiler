#include <stdlib.h>
#include <math.h>
#include "./defines.h"
#include "./mpctools.h"

void init_archive_data(ArchiveDataPtr ad, long number_of_u, long number_of_y, float default_u, float default_y, float current_yzad){
	int i;
	ad->number_of_u = number_of_u;
	ad->number_of_y = number_of_y;
	ad->u  = (float*)malloc(sizeof(float)*ad->number_of_u);
	ad->du = (float*)malloc(sizeof(float)*ad->number_of_u);
	ad->y  = (float*)malloc(sizeof(float)*ad->number_of_y);
	for(i=1; i<=ad->number_of_u; ++i) ad->u[M(i)] = default_u;
	for(i=1; i<=ad->number_of_u; ++i) ad->du[M(i)] = 0;
	for(i=1; i<=ad->number_of_y; ++i) ad->y[M(i)] = default_y;
	ad->uk1  = &( ad->u[ad->number_of_u+1]);
	ad->duk1 = &(ad->du[ad->number_of_u+1]);
	ad->yk   = &( ad->y[ad->number_of_y]);
	ad->yzad = current_yzad;
}
void new_control(ArchiveDataPtr ad, float u){
	int i;
	for(i=1; i<=(ad->number_of_u-1); ++i) {
		ad->u[M(i)] = ad->u[M(i+1)];
		ad->du[M(i)] = ad->du[M(i+1)];
	}
	ad->u[M(ad->number_of_u)] = u;
	ad->du[M(ad->number_of_u)] = u - ad->u[M(ad->number_of_u-1)];
}
void new_control_increment(ArchiveDataPtr ad, float du){
	int i;
	for(i=1; i<=(ad->number_of_u-1); ++i) {
		ad->u[M(i)] = ad->u[M(i+1)];
		ad->du[M(i)] = ad->du[M(i+1)];
	}
	ad->u[M(ad->number_of_u)] = ad->u[M(ad->number_of_u-1)]+du;
	ad->du[M(ad->number_of_u)] = du;
}
void new_output(ArchiveDataPtr ad, float y){
	int i;
	
	for(i=1; i<=(ad->number_of_y-1); ++i) ad->y[M(i)] = ad->y[M(i+1)];
	ad->y[M(ad->number_of_y)] = y;
}
void new_simulated_signal(ArchiveDataPtr ad, float y){
	#ifdef HARDWARE_SIMULATION 	// if hardware, then we have to overwrite last signal value with new signal value
		printf("%d\n",ad);
		printf("%d\n",ad->number_of_u);
		ad->y[M(ad->number_of_y)] = y;
	#else			// if software, then we have to append new signal value
		new_output(ad,y);
	#endif
}
void setCurrentControlIncrement(CurrentControlPtr c,float du){
	c->du = du;
	c->type = INC;
}
void setCurrentControl(CurrentControlPtr c,float u){
	c->u = u;
	c->type = ABS;
}
void pushCurrentControlsToArchiveData(CurrentControlPtr  c,ArchiveDataPtr  ad){
	if(c->type == ABS) 	new_control(ad, c->u);
	else				new_control_increment(ad, c->du);
}
void CST_control(ArchiveDataPtr  ad, CurrentControlPtr  c){
	if(ad == NULL) return;
	setCurrentControl(c,-1.000000e-01); // algorytm przyrostowy
}
void projectOnFeasibleSet(ArchiveDataPtr ad, float dumin, float dumax, float umin, float umax){
	projectControlsIncrementOnFeasibleSet(ad, dumin, dumax);
	projectControlsOnFeasibleSet(ad, umin, umax);
}
void projectControlsOnFeasibleSet(ArchiveDataPtr ad, float umin, float umax){
	float diff = 0.0;
	if(umin != NAN && ad->uk1[M(-1)] < umin){
		diff = umin - ad->uk1[M(-1)];
		ad->uk1[M(-1)] = umin;		
	}
	if(umax != NAN && ad->uk1[M(-1)] > umax){
		diff = umax - ad->uk1[M(-1)];
		ad->uk1[M(-1)] = umax;
	}
	ad->duk1[M(-1)] += diff;
}
void projectControlsIncrementOnFeasibleSet(ArchiveDataPtr ad, float dumin, float dumax){
	float diff = 0.0;
	if(dumin != NAN && ad->duk1[M(-1)] < dumin){
		diff = dumin - ad->duk1[M(-1)];
		ad->duk1[M(-1)] = dumin;		
	}
	if(dumax != NAN && ad->duk1[M(-1)] > dumax){
		diff = dumax - ad->duk1[M(-1)];
		ad->duk1[M(-1)] = dumax;
	}
	ad->uk1[M(-1)] += diff;
}
float last_control(ArchiveDataPtr ad){
	return ad->uk1[M(-1)];
}
float last_control_increment(ArchiveDataPtr ad){
	return ad->duk1[M(-1)];
}

