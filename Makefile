all:
	g++ main.cpp -o ./crosscompiler -I/usr/local/MATLAB/R2012a/extern/include -L/usr/local/MATLAB/R2012a/bin/glnxa64 -lmx -leng

clean:
	rm -rf ./crosscompiler
	rm -rf *tmp.m
