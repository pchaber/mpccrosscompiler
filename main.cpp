#include <iostream>
#include <fstream>
#include <sstream>
#include <unistd.h>
#include <algorithm>
#include <cstring>

#include <stdio.h>
#include <stdlib.h>
#include "engine.h"

#define BLOCK_OPEN "#MPC_BEGIN"
#define BLOCK_CLOSE "#MPC_END"

#define BLOCK_INCLUDE "#MPC_INCLUDE"

#define PROFILER_BEGIN "#MPC_PROFILER_BEGIN"
#define PROFILER_COUNT "#MPC_PROFILER_COUNT"
#define PROFILER_END "#MPC_PROFILER_END"

#define  BUFSIZE 1000000

using namespace std;

int main (int argc, char **argv){
	char c;
	
	string input_file_name = "";	// name of file to crosscompile
	string output_file_name = "";	// name of crossompiled file
	string m_libraries_path = "";	// path of MATLAB libraries
	string c_libraries_path = "";	// path of MATLAB libraries
	string simulate_function = "sim_const";  // name of function used to simulate
	
	bool profiler = false;			// do we use profiler?
	bool simulation = false;
	bool software_simulation = false;
	bool hardware_simulation = false;
	bool verbose = false;
	bool interrupt = false;
	bool nodelay = false;

	long iteration_time = 100;	// time of one iteration (needed for one of the usage of crosscompiler)
	long max_time = -1;


	opterr = 0;
	while ((c = getopt (argc, argv, "Ivs:h:pt:T:i:o:l:L:d")) != -1){
		switch (c){
			case 'i': // input name
				input_file_name = optarg;
				break;
			case 'I': 
				interrupt = true;
				break;
			case 'v':
				verbose = true;
				break;
			case 'p': // use profiler?
				profiler = true;
				break;
			case 'd':
				nodelay = true;
				break;
			case 's':
				simulation = true;
				software_simulation = true;
				hardware_simulation = false;
				simulate_function = optarg;
				break;
			case 'h':
				simulation = true;
				software_simulation = false;
				hardware_simulation = true;
				simulate_function = optarg;
				break;
			case 't': // iteration time
				iteration_time = atoi(optarg);
				break;
			case 'T':
				max_time = atoi(optarg);
				break;
			case 'o': // output name
				output_file_name = optarg;
				break;
			case 'l': // output name
				m_libraries_path = optarg;
				break;
			case 'L': // output name
				c_libraries_path = optarg;
				break;
			case '?': // error interpreting options
				cout << "[EE] Bad arguments." << endl;
				return 1;
			default:
				break;
		}
	}
	if(input_file_name.empty()){
		cout << "[EE] No input file given. " << endl;
		return 1;
	} else {
		cout << "[II] Input file: " << input_file_name << endl;					// logs
	}
	if(output_file_name.empty()){
		cout << "[EE] No output file given. " << endl;
		return 1;
	} else {
		cout << "[II] Output file: " << output_file_name << endl;	// logs
	}
	if(m_libraries_path.empty()){
		m_libraries_path = "./Libs/MATLAB/";
	}
    char resolved_path[PATH_MAX]; 
    realpath(m_libraries_path.c_str(), resolved_path); 
    m_libraries_path = resolved_path;
	cout << "[II] MATLAB libraries path set to " << m_libraries_path << endl;
	if(c_libraries_path.empty()){
		c_libraries_path = "./Libs/C/";
	}
    realpath(c_libraries_path.c_str(), resolved_path); 
    c_libraries_path = resolved_path;
	cout << "[II] C libraries path set to " << c_libraries_path << endl;

	cout << "[II] Creating set of defines..." << flush;
	string defines_filename = c_libraries_path+"/defines.h";
	ofstream 	defines_file(defines_filename.c_str());						// open output file as output stream
	defines_file << "#ifndef __DEFINES_H__" << endl << "#define __DEFINES_H__" << endl;
	if(verbose)					defines_file << "#define VERBOSE" << endl;
	if(interrupt)				defines_file << "#define MPC_INTERRUPT" << endl;
	if(simulation)				defines_file << "#define SIMULATION" << endl;
	if(simulation)				defines_file << "#define SIMULATE_FUNCTION " << simulate_function << endl;
	if(hardware_simulation)		defines_file << "#define HARDWARE_SIMULATION" << endl;
	if(software_simulation)		defines_file << "#define SOFTWARE_SIMULATION" << endl;
	if(nodelay)					defines_file << "#define TAKE_YOUR_TIME" << endl;
	if(max_time >= 0)			defines_file << "#define MAX_TIME " << max_time << endl;
	if(!interrupt)				defines_file << "#define ITERATION_TIME " << iteration_time << endl;
	defines_file << "#endif" << endl;
	cout << "OK" << endl;

	string tmp_input_file_name = input_file_name;							// copy input file name to tmpe input file name
	replace(tmp_input_file_name.begin(),tmp_input_file_name.end(),'.','_'); // replace all the dots to underscores (MATLAB doesn't like dots since filename is at the same time a function name)
	tmp_input_file_name.append("_tmp.m");									// add to input file name the _tmp.m ending
	cout << "[II] Temporary input file: " << tmp_input_file_name << endl;	// logs

	cout << "[II] Opening input and output files..." << flush;
	ifstream     input_file(input_file_name.c_str());						// open input file as input stream
	ofstream 	output_file(output_file_name.c_str());						// open output file as output stream
	ofstream tmp_input_file;												// prepare to open tmp input file as output stream
	cout << "OK" << endl;

	cout << "[II] MATLAB engine initialisation..." << flush;
	Engine *ep;
	char buffer_with_prompt[BUFSIZE+1+3];
	char * buffer = buffer_with_prompt+3;
	if (!(ep = engOpen("matlab -nosplash -nodesktop -nodisplay"))) {
		cout << "[EE] Can't start MATLAB engine" << endl;
		return -1;
	} cout << "OK" << endl;

	engOutputBuffer(ep, buffer_with_prompt, BUFSIZE);
	engEvalString(ep, "disp(pwd);");
	cout << "[II] Current working directory: " << buffer << flush; // buffer contains \n

	cout << "[II] Adding MATLAB library paths..." << flush;
	string library_path = m_libraries_path;
	string addpath_command = "addpath('" + library_path + "');";
	engEvalString(ep, addpath_command.c_str());
	addpath_command = "addpath(cd(cd('./')));";
	engEvalString(ep, addpath_command.c_str());
	cout << "OK" << endl;
	cout << "[II] Setting global flag MPC_CROSSCOMPILER..." << flush;
	string global_command = "global MPC_CROSSCOMPILER; MPC_CROSSCOMPILER = 1;";
	cout << "OK" << endl;
	engEvalString(ep, global_command.c_str());

	output_file << "#include \""<< c_libraries_path << "/defines.h\"" << endl;
	if(profiler) output_file << "#include \""<< c_libraries_path << "/profiler.h\"" << endl;
	output_file << "#include \""<< c_libraries_path << "/mpctools.h\"" << endl;
	output_file << "#include \""<< c_libraries_path << "/simulated_signals.h\"" << endl;
	output_file << "#include \""<< c_libraries_path << "/default_functions.h\"" << endl;

	string line;
	bool block_open = false;
	while (getline(input_file, line)){
		// cout << "--> " << line << endl;
		istringstream iss(line);
		string word;
		if(!(iss >> word)) {
			word = "";
		} // if there is no word

		if(!block_open && word.compare(BLOCK_OPEN)==0){ // if there is a block opening keyword
			tmp_input_file.open(tmp_input_file_name.c_str(), ios_base::out);
			while(!tmp_input_file.is_open());
			block_open = true;
		} else if (block_open && word.compare(BLOCK_CLOSE)==0){ // if there is a block closing keyword
			block_open = false;
			tmp_input_file.close();
			while(tmp_input_file.is_open());
			string run_command = "run ./" + tmp_input_file_name + ";";
			memset(buffer_with_prompt,0,BUFSIZE+1);
			engEvalString(ep, run_command.c_str());
			// cout << buffer << endl;
			output_file << buffer;
		} else if(block_open){ // if the block is opened - we are reading it line by line
			tmp_input_file << line << endl;
		} else if(word.compare(BLOCK_INCLUDE)==0){
			iss >> word;
			string run_command = "run ./" + word.substr(1,word.size()-4) + ";";
			memset(buffer_with_prompt,0,BUFSIZE+1);
			engEvalString(ep, run_command.c_str());
			output_file << buffer;
		} else if(word.compare(PROFILER_BEGIN)==0){
			iss >> word;
			int id = atoi(word.c_str());
			if(profiler) {
				output_file << "profiler_start("<<id<<");" << endl;
			}
		} else if(word.compare(PROFILER_COUNT)==0){
			iss >> word;
			int id = atoi(word.c_str());
			if(profiler) {
				output_file << "profiler_count("<<id<<");" << endl;
			}
		} else if(word.compare(PROFILER_END)==0){
			iss >> word;
			int id = atoi(word.c_str());
			if(profiler) {
				output_file << "profiler_end("<<id<<");" << endl;
			}
		} else {
			output_file << line << endl;
		}

	}
	input_file.close();
	engEvalString(ep, "clear MPC_CROSSCOMPILER;");
	engClose(ep);

	cout << "[II] End of cross compilation." << endl << endl;

	return 0;
}

